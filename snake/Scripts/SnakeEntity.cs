using Godot;
using System;

public class SnakeEntity
{
    public float score = 0f;
    public int ticksCountToTimeout = 0;
    public int ticksAlive = 0;
    public bool dead = false;
    public bool deathByTimeout = false;
    public NeuralNetwork neuralNetwork;
    public Snake snake;
    public Node snakeNode;
    public SnakeEntity(Snake snake, Node snakeNode, NeuralNetwork neuralNetwork)
    {
        this.snake = snake;
        this.snakeNode = snakeNode;
        this.neuralNetwork = neuralNetwork;
    }
    public void Move()
    {
        var output = neuralNetwork.FeedForward(snake.GetDataPoints());
        // Get largest index
        float max = output[0];
        int maxIndex = 0;
        for (int i = 1; i < output.Length; i++)
        {
            if (output[i] > max)
            {
                max = output[i];
                maxIndex = i;
            }
        }
        switch ((int)snake.direction.Angle())
        {
            // Right
            case 0:
                switch (maxIndex)
                {
                    // Left
                    case 0:
                        snake.direction = new Vector2(0, -Snake.width);
                        break;
                    case 1:
                        break;
                    // Right
                    case 2:
                        snake.direction = new Vector2(0, Snake.width);
                        break;
                }
                break;
            // Down
            case 1:
                switch (maxIndex)
                {
                    // Left
                    case 0:
                        snake.direction = new Vector2(Snake.width, 0);
                        break;
                    case 1:
                        break;
                    // Right
                    case 2:
                        snake.direction = new Vector2(-Snake.width, 0);
                        break;
                }
                break;
            // Left
            case 3:
                switch (maxIndex)
                {
                    // Left
                    case 0:
                        snake.direction = new Vector2(0, Snake.width);
                        break;
                    case 1:
                        break;
                    // Right
                    case 2:
                        snake.direction = new Vector2(0, -Snake.width);
                        break;
                }
                break;
            // Up
            case -1:
                switch (maxIndex)
                {
                    // Left
                    case 0:
                        snake.direction = new Vector2(-Snake.width, 0);
                        break;
                    case 1:
                        break;
                    // Right
                    case 2:
                        snake.direction = new Vector2(Snake.width, 0);
                        break;
                }
                break;
        }
        snake.Move();
    }
    public void DrawSnake()
    {
        if (snake.body.Count > snakeNode.GetChildCount())
        {
            var lastChild = snakeNode.GetChild(snakeNode.GetChildCount() - 1).Duplicate();
            lastChild.Name = "body " + snakeNode.GetChildCount();
            snakeNode.AddChild(lastChild);
        }
        for (int i = 0; i < snake.body.Count; i++)
        {
            snakeNode.GetChild<ColorRect>(i).Visible = true;
            snakeNode.GetChild<ColorRect>(i).Position = snake.body[i];
        }
    }
}
