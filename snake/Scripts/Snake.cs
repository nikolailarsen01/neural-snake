using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class Snake
{
    public Vector2 direction;
    public List<Vector2> body = new List<Vector2>();
    List<Vector2> bodyCopy = new List<Vector2>();
    public const int width = 40;
    Vector2 widthVector = new Vector2(width, width);
    public bool isAppleCollide;
    public float[] sensorDirection = new float[] { 0, 0, 0, 0 };
    public float[] sensorWall = new float[] { 0, 0, 0 };
    public float[] sensorFood = new float[] { 0, 0 };
    public float[] sensorHead = new float[] { 0, 0 };
    public Snake()
    {
        // Random spawn, but cannot spawn directly at the edge
        var rand = new Random();
        var x = rand.Next(1, 20) * width;
        var y = rand.Next(1, 20) * width;

        body.Add(new Vector2(x, y));
        //Set direction
        var dirX = 0;
        var dirY = 0;

        switch (rand.Next(0, 4))
        {
            //Right
            case 0:
                dirX = width;
                dirY = 0;
                break;
            //Down
            case 1:
                dirX = 0;
                dirY = width;
                break;
            //Left
            case 2:

                dirX = -width;
                dirY = 0;
                break;
            //Up
            case 3:
                dirX = 0;
                dirY = -width;
                break;
        }
        body.Add(new Vector2(x - dirX, y - dirY));

        direction = new Vector2(dirX, dirY);

        isAppleCollide = false;
    }
    public float[] GetDataPoints()
    {
        return sensorDirection.Concat(sensorWall).Concat(sensorFood).Concat(sensorHead).ToArray();
    }
    public void UpdateSensors(Vector2I windowBorder, Vector2 applePos)
    {
        for (int i = 0; i < 3; i++)
        {
            sensorWall[i] = 0;
        }
        for (int i = 0; i < 4; i++)
        {
            sensorDirection[i] = 0;
        }
        var normalizeValue = 0.1f;
        // Head sensor
        sensorHead[0] = body[0].X * normalizeValue;
        sensorHead[1] = body[0].Y * normalizeValue;
        // Food sensor
        sensorFood[0] = applePos.X * normalizeValue;
        sensorFood[1] = applePos.Y * normalizeValue;
        // Wall sensor
        switch ((int)direction.Angle())
        {
            // Right
            case 0:
                sensorDirection[0] = 1;
                // Left
                if (body[0].Y == 0) sensorWall[0] = 1;
                // In Front
                if (body[0].X + widthVector.X == windowBorder.X) sensorWall[1] = 1;
                // Right
                if (body[0].Y + width == windowBorder.Y) sensorWall[2] = 1;
                break;
            // Down
            case 1:
                sensorDirection[1] = 1;
                // Left
                if (body[0].X + widthVector.X == windowBorder.X) sensorWall[0] = 1;
                // In Front
                if (body[0].Y + widthVector.Y == windowBorder.Y) sensorWall[1] = 1;
                // Right
                if (body[0].X == 0) sensorWall[2] = 1;
                break;
            // Left
            case 3:
                sensorDirection[2] = 1;
                // Left
                if (body[0].Y + widthVector.Y == windowBorder.Y) sensorWall[0] = 1;
                // In Front
                if (body[0].X == 0) sensorWall[1] = 1;
                // Right
                if (body[0].Y == 0) sensorWall[2] = 1;
                break;
            // Up
            case -1:
                sensorDirection[3] = 1;
                // Left
                if (body[0].X == 0) sensorWall[0] = 1;
                // In Front
                if (body[0].Y == 0) sensorWall[1] = 1;
                // Right
                if (body[0].X + widthVector.X == windowBorder.X) sensorWall[2] = 1;
                break;
        }
        //GD.Print($"Left: {sensorWall[0]} In front: {sensorWall[1]} Right: {sensorWall[2]}");
    }
    public void Move()
    {
        if (isAppleCollide)
        {
            bodyCopy = body.GetRange(0, body.Count);
            isAppleCollide = false;
        }
        else
        {
            bodyCopy = body.GetRange(0, body.Count - 1);
        }
        var newHead = bodyCopy[0] + direction;
        bodyCopy.Insert(0, newHead);
        body = bodyCopy;
    }
}
