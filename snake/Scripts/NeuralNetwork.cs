using System.Collections;
using System;
using System.Collections.Generic;
public class NeuralNetwork : IComparable<NeuralNetwork>
{
    private int[] layers;
    private float[][] neurons;
    private float[][] hiddenBiases;
    public float[][][] weights;
    public float fitness;
    Random rand = new Random();

    public NeuralNetwork(int[] layers)
    {
        this.layers = new int[layers.Length];
        for (int i = 0; i < layers.Length; i++)
        {
            this.layers[i] = layers[i];
        }

        InitNeurons();
        InitWeights();
        InitBiases();
    }

    public NeuralNetwork(NeuralNetwork copyNetwork)
    {
        this.fitness = copyNetwork.fitness;
        this.layers = new int[copyNetwork.layers.Length];
        for (int i = 0; i < copyNetwork.layers.Length; i++)
        {
            this.layers[i] = copyNetwork.layers[i];
        }
        InitNeurons();
        InitWeights();
        InitBiases();
        CopyWeights(copyNetwork.weights);
    }

    public void CopyWeights(float[][][] copyWeights)
    {
        for (int i = 0; i < weights.Length; i++)
        {
            for (int j = 0; j < weights[i].Length; j++)
            {
                for (int k = 0; k < weights[i][j].Length; k++)
                {
                    weights[i][j][k] = copyWeights[i][j][k];
                }
            }
        }
    }

    private void InitNeurons()
    {
        var neuronsList = new List<float[]>();

        for (int i = 0; i < layers.Length; i++)
        {
            neuronsList.Add(new float[layers[i]]);
        }

        neurons = neuronsList.ToArray();
    }
    private void InitBiases()
    {
        var hiddenBiasesList = new List<float[]>();
        for (int i = 1; i < layers.Length - 1; i++)
        {
            var layerBiases = new float[layers[i]];
            for (int j = 0; j < layers[i]; j++)
            {
                layerBiases[i] = (float)(rand.NextDouble() * 2 - 1);
            }
            hiddenBiasesList.Add(layerBiases);
        }
        hiddenBiases = hiddenBiasesList.ToArray();
    }

    private void InitWeights()
    {
        List<float[][]> weightsList = new List<float[][]>();

        for (int i = 1; i < layers.Length; i++)
        {
            var layerWeightsList = new List<float[]>();

            int neuronsInPreviousLayer = layers[i - 1];

            for (int j = 0; j < neurons[i].Length; j++)
            {
                float[] neuronWeights = new float[neuronsInPreviousLayer];

                //weight starts as a random value between 0.5f and -0.5f
                for (int k = 0; k < neuronsInPreviousLayer; k++)
                {
                    //give random weights to neuron weights
                    neuronWeights[k] = (float)(rand.NextDouble() - .5f);
                }

                layerWeightsList.Add(neuronWeights);
            }

            weightsList.Add(layerWeightsList.ToArray());
        }

        weights = weightsList.ToArray();
    }
    public float[] FeedForward(float[] inputs)
    {

        for (int i = 0; i < inputs.Length; i++)
        {
            neurons[0][i] = inputs[i];
        }

        for (int i = 1; i < layers.Length; i++)
        {
            for (int j = 0; j < neurons[i].Length; j++)
            {
                float value = 0f;

                for (int k = 0; k < neurons[i - 1].Length; k++)
                {
                    value += weights[i - 1][j][k] * neurons[i - 1][k];
                }
                neurons[i][j] = (float)(1 / (1 + Math.Exp(-value + hiddenBiases[i - i][j])));
            }
        }
        return neurons[neurons.Length - 1];
    }
    public void Mutate()
    {
        //Mutate weights
        for (int i = 0; i < weights.Length; i++)
        {
            for (int j = 0; j < weights[i].Length; j++)
            {
                for (int k = 0; k < weights[i][j].Length; k++)
                {
                    float weight = weights[i][j][k];
                    float randomNumber = rand.Next(0, 20);

                    if (randomNumber <= 2)
                    {
                        weight *= -1f;
                    }
                    else if (randomNumber <= 4)
                    {
                        weight += (float)(rand.NextDouble() * 0.02f - 0.01f);
                    }
                    else if (randomNumber <= 6)
                    {
                        float factor = (float)(rand.NextDouble() * 0.05f) + 0.05f;
                        weight += factor;
                    }
                    else if (randomNumber <= 8)
                    {
                        float factor = (float)(rand.NextDouble() * 0.9f + 0.1f);
                        weight += factor;
                    }
                    weights[i][j][k] = weight;
                }
            }
        }
        //Mutate hidden biases
        for (int i = 0; i < hiddenBiases.Length; i++)
        {
            for (int j = 0; j < hiddenBiases[i].Length; j++)
            {
                float bias = hiddenBiases[i][j];
                float randomNumber = rand.Next(0, 20);

                if (randomNumber <= 5)
                {
                    bias += (float)(rand.NextDouble() * 0.02f - 0.01f);
                }
                else if (randomNumber <= 10)
                {
                    bias += (float)(rand.NextDouble() * 0.2f - 0.1f);
                }
                else if (randomNumber <= 15)
                {
                    bias += (float)(rand.NextDouble() * 1f - 0.5f);
                }
                hiddenBiases[i][j] = bias;
            }
        }
    }
    public void AddFitness(float fit)
    {
        fitness += fit;
    }

    public void SetFitness(float fit)
    {
        fitness = fit;
    }

    public float GetFitness()
    {
        return fitness;
    }

    public int CompareTo(NeuralNetwork other)
    {
        if (other == null) return 1;

        if (fitness > other.fitness)
            return 1;
        else if (fitness < other.fitness)
            return -1;
        else
            return 0;

    }
}
