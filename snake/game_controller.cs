using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class game_controller : Node2D
{
    Node snakeNode;
    Sprite2D apple;
    Label info;
    Vector2I windowBorder;
    NeuralNetwork bestNeuralNetwork;
    List<SnakeEntity> snakes;
    int numOfSnakes = 100;
    // Number of ticks to kill, if it has not eaten
    int timeout = 60;
    int ticks = 0;
    int epoch = 1;
    int alive = 0;
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        alive = numOfSnakes;

        snakeNode = GetNode<Node>("snake");
        snakes = new List<SnakeEntity>();
        for (int i = 0; i < numOfSnakes; i++)
        {
            var snake = new Snake();
            var neuralNetwork = new NeuralNetwork(new int[] { 11, 10, 3 });
            neuralNetwork.SetFitness(0f);
            var node = snakeNode.Duplicate();
            this.AddChild(node);
            snakes.Add(new SnakeEntity(snake, node, neuralNetwork));
        }
        windowBorder = DisplayServer.WindowGetSize();
        apple = GetNode<Sprite2D>("apple");

        info = GetNode<Label>("info");
        bestNeuralNetwork = new NeuralNetwork(new int[] { 11, 10, 3 });
        bestNeuralNetwork.SetFitness(0f);
        DrawApple();
        apple.Visible = true;
    }
    public void DrawInfo()
    {
        info.Text = $"Highest score: {snakes.OrderBy(x => x.score).Select(x => x.score).First()}; Fitness: {bestNeuralNetwork.GetFitness()}; Epoch: {epoch}; Alive: {alive}";
    }
    public void ResetSingle(SnakeEntity snakeEntity)
    {
        // Calculations
        if (snakeEntity.deathByTimeout)
        {
            snakeEntity.neuralNetwork.SetFitness(-100f);
        }
        else
        {
            float fitness = snakeEntity.score + (snakeEntity.ticksAlive * .0001f);
            snakeEntity.neuralNetwork.SetFitness(fitness);
        }

        if (snakeEntity.neuralNetwork.GetFitness() > bestNeuralNetwork.GetFitness())
        {
            bestNeuralNetwork = new NeuralNetwork(snakeEntity.neuralNetwork);
        }
        else if (snakeEntity.neuralNetwork.GetFitness() < bestNeuralNetwork.GetFitness())
        {
            snakeEntity.neuralNetwork = new NeuralNetwork(bestNeuralNetwork);
        }
        else
        {
            var rand = new Random();
            switch (rand.Next(0, 2))
            {
                case 0:
                    bestNeuralNetwork = new NeuralNetwork(snakeEntity.neuralNetwork);
                    break;
                case 1:
                    snakeEntity.neuralNetwork = new NeuralNetwork(bestNeuralNetwork);
                    break;
            }
        }
        // Rest of reset
        alive--;
        int length = snakeEntity.snakeNode.GetChildCount();
        if (length > 2)
        {
            var children = new List<Node>();
            for (int i = 2; i < length; i++)
            {
                children.Add(snakeEntity.snakeNode.GetChild(i));
            }
            foreach (var child in children)
            {
                snakeEntity.snakeNode.RemoveChild(child);
            }
        }
        snakeEntity.deathByTimeout = false;
        snakeEntity.ticksCountToTimeout = 0;
        snakeEntity.score = 0f;
        snakeEntity.ticksAlive = 0;
        snakeEntity.snake = new Snake();
    }
    public void Reset(SnakeEntity snakeEntity)
    {
        if (alive == 1) ResetLast(snakeEntity);
        else ResetSingle(snakeEntity);
    }
    public void ResetLast(SnakeEntity snakeEntity)
    {
        epoch++;
        //GD.Print($"best fitness: {bestNeuralNetwork.GetFitness()}; this fitness: {neuralNetwork.GetFitness()};");
        ResetSingle(snakeEntity);
        //Copy to snakes and finish reset
        foreach (var snake in snakes)
        {
            snake.neuralNetwork = new NeuralNetwork(bestNeuralNetwork);
            snake.neuralNetwork.Mutate();
            snake.dead = false;
        }
        alive = numOfSnakes;
        ticks = 0;
        DrawApple();
        apple.Visible = true;
    }


    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(double delta)
    {
        Tick();
    }
    public Vector2 GetRandPosForApple()
    {
        var rand = new Random();
        var x = (rand.Next() % 20) * Snake.width;
        var y = (rand.Next() % 20) * Snake.width;
        return new Vector2(x, y);
    }
    #region Draw
    public void DrawApple()
    {
        var newRandPos = GetRandPosForApple();
        foreach (var snake in snakes)
        {
            foreach (var block in snake.snake.body)
            {
                if (block == newRandPos)
                {
                    newRandPos = GetRandPosForApple();
                    continue;
                }
                if (block == snake.snake.body[snake.snake.body.Count - 1])
                {
                    apple.Position = newRandPos;
                }
            }
        }
    }
    #endregion
    bool IsAppleCollide(SnakeEntity snakeEntity)
    {
        if (snakeEntity.snake.body[0] == apple.Position)
        {
            snakeEntity.score++;
            return true;
        }
        else return false;
    }

    /*public void input()
    {
        if (Input.IsActionPressed("ui_right")
            && snake.direction != new Vector2(-Snake.width, 0)) snake.direction = new Vector2(Snake.width, 0);
        else if (Input.IsActionPressed("ui_left")
            && snake.direction != new Vector2(Snake.width, 0)) snake.direction = new Vector2(-Snake.width, 0);
        else if (Input.IsActionPressed("ui_down")
            && snake.direction != new Vector2(0, -Snake.width)) snake.direction = new Vector2(0, Snake.width);
        else if (Input.IsActionPressed("ui_up")
            && snake.direction != new Vector2(0, Snake.width)) snake.direction = new Vector2(0, -Snake.width);
    }*/

    public bool IsGameOver(Snake snake)
    {
        if (snake.body[0].X < 0 || snake.body[0].X > windowBorder.X - Snake.width) return true;
        else if (snake.body[0].Y < 0 || snake.body[0].Y > windowBorder.Y - Snake.width) return true;
        if (snake.body.Count >= 3)
        {
            foreach (var block in snake.body.GetRange(1, snake.body.Count - 1))
            {
                if (snake.body[0] == block)
                {
                    return true;
                }
            }
        }
        return false;
    }
    private void Tick()
    {
        foreach (var snakeEntity in snakes)
        {
            if (snakeEntity.dead == true) ;
            else
            {
                if (IsGameOver(snakeEntity.snake))
                {
                    //GD.Print($"Wall infront: {snake.sensorWall[1]}; Window X: {windowBorder.X}; Head X: {snake.body[0].X};");
                    Reset(snakeEntity);
                }
                else
                {
                    if (snakeEntity.ticksCountToTimeout == timeout)
                    {
                        snakeEntity.deathByTimeout = true;
                        Reset(snakeEntity);
                    }
                    else
                    {
                        snakeEntity.ticksAlive++;
                        snakeEntity.ticksCountToTimeout++;
                        snakeEntity.snake.UpdateSensors(windowBorder, apple.Position);
                        snakeEntity.Move();
                        snakeEntity.DrawSnake();
                        if (IsAppleCollide(snakeEntity))
                        {
                            DrawApple();
                            snakeEntity.snake.isAppleCollide = true;
                        }
                    }
                }
            }
        }
        ticks++;
        DrawInfo();
    }
}
